## Gradle + STS

## 목차
### 🙋‍♂️ 요구사항 해결 방법
- [TODO 1. XML 문서로 작성된 영화 메타데이터도 다룰 수 있게 기능을 확장하라](#요구사항-첫번째)
- [TODO 2. 영화 메타데이터 위치를 변경할 수 있도록 하라](#요구사항-두번째)
- [TODO 3. 영화 메타데이터 읽기 속도를 빠르게 하라](#요구사항-세번째)

### 사용 기술 및 프로그램
- Gradle 7.x
- Spring Tool Suite(IDE) - `STS`

### 추가 요구사항
- * TODO 1. XML 문서로 작성된 영화 메타데이터도 다룰 수 있게 기능을 확장하라
- * TODO 2. 영화 메타데이터 위치를 변경할 수 있도록 하라
- * TODO 3. 영화 메타데이터 읽기 속도를 빠르게 하라
- * TODO 4. 시스템 언어설정에 따라 애플리케이션 메시지가 영어 또는 한글로 출력되게 하라

## 요구사항 첫번째
#### 1. 기본 프로젝트 구조 `God object 안티 패턴`
```
📦 moviebuddy
├─ ApplicationException.java ✅ 런타임 클래스를 상속받아 애플리케이션에서 발생하는 오류를 다루기 위한 ul 클래스 ㄹ
├─ Movie.java ✅ 데이터를 담아두는 자바 빈 클래스데
├─ MovieBuddyApplication.java ✅ 핵심 비즈니스 로직
├─ util
│  └─ FileSystemUtils.java 
└─ src/main/resources
   ├─ movie_metadata.csv
   └─ movie_metadata.xml
```
##### 1-1. 문제점
- 클래스가 커지면서 `클래스의 목적` 이 무엇인지 이해하기 어려워짐
- 코드가 불안정하고 변화에 쉽게 망가질 수 있다
- 거대 클래스가 모든 것을 처리하기 때문에 애플리케이션이 그 역할이 `신` 과 같아 >> ✅ `God object 안티 패턴`

##### 1-2. 프로그래밍 기술 정의
- 큰 문제를 여러 개의 작은 문제로 분리하고 각각에 대한 해결책을 만드는 것 
- 작은 문제를 해결하는 것 >> 주어진 문제와 해결 방법에 대해서만 알면 된다

#### 2. `관심사의 분리` 와 `계층화` 
1. 도메인 관심사 분리
	1. 도메인 클래스 생성 및 Movie 클래스 이동
	2. 영화 목록 불러오기 및 영화를 검색하는 코드 이동
		- MovieFinder 클래스 생성
	3. 결과
		- MovieBuddyApplication.java > 사용자 입력을 받아들이고 그걸 통해서 명령을 수행하고 결과를 출력하는 관심사만 남는다 ! 
		- MovieFinder.java > 영화 검색하는 기능 관심사 이동

```
📦 moviebuddy
├─ ApplicationException.java ✅ Presentation
├─ domain ✅ Domain
│  ├─ Movie.java
│  └─ MovieFinder.java
├─ ...
└─ src/test/java
   └─ moviebuddy
      └─ domain
         └─ MovieFinderTest.java
```

##### 2-1. 요구사항 해결
##### `XML 문서로 작성된 영화 메타데이터도 다룰 수 있게 기능을 확장하라`
- 기존 MovieFinder.java >> csv 다루는 기능
	- 해결 방법
		- 분기문 이용 ? >> ✅기존 코드를 수정하면 버그 발생!
		- 버그 추가 불확실성 기인

- ✅`상속` `다형성` 이용해서 해결하기 >> ⭐ `추상화`
	- 🙋‍♂️ 어떤 것들의 `공통적인 속성` 을 뽑아내서 이를 따로 분리해내는 작업
	- MovieFinder.java >> 추상 클래스 변경 / loadMovies 메서드 >> 추상 메서드 변경
	
- ✅ `템플릿 메서드 패턴`
	- 부모 클래스의 기본적인 알고리즘의 흐름을 구현하고 중간에 필요한 처리를 자식 클래스에게 위임하는 구조
	
```
📦 moviebuddy
└─ MovieFinder.java <<abstract>>
   ├─ CsvMovieFinder.java
   └─ XmlMovieFinder.java
```

- ✅ `합성` 이용해 >> `추상화`
	- 기존 문제점 `상속`
		1. 캡슐화 위반
		2. 설계를 유연하지 못하게 만든다는 것
	- 따라서, 코드를 재사용하기 위해서는 상속보다는 `합성` 먼저 고려
	- ⭐ `합성` : 다른 객체의 인스턴스를 자신의 인스턴스 변수를 포함해서 재사용하는 방법 

```
📦 moviebuddy
└─ MovieFinder.java 
   └─ MovieReader <<interface>> ✅ 다양한 형식의 메타데이터를 읽을 수 있도록 확장할 수 있는 코드 
      ├─ CsvMovieReader.java
      └─ XmlMovieReader.java
```
 

#### 3. JAXB(Java Architecture for XML Binding)
> XML문서를 자바객체와 매핑하는 기술

##### 3-1. Unmarshalling `XML 문서 > 자바 객체`
1. 의존성 추가

```java
implementation 'org.glassfish.jaxb:jaxb-runtime:2.3.1'
```

2. JaxbMovieReader 클래스 생성 및 XML 문서 대응하는 자바 객체 생성
	- xml 문서 매핑 어노테이션
		- `@XmlRootElement(name = "moviemetadata")`

#### 4. 테스트
##### 4-1. JUnit
> 테스트 도와주는 도구
1. 의존성 추가

```java
testImplementation 'org.junit.jupiter:junit-jupiter-engine:5.5.2'
testRuntimeOnly 'org.junit.platform:junit-platform-launcher:1.5.1'
```

2. Gradle에 test 설정 

```java
test {
	useJunitPlatform()
}
```

#### 5. 추상화 후 문제점
- 여전히 MovieFinder 클래스도 같이 변경되고 있다.
- 해결 방법
	- MovieFinder 외부에서 책임질 수 있도록 분리
	- 생성자를 통해 외부에서 입력 받도록

```java
private final MovieReader movieReader;

public MovieFinder(MovieReader movieReader) {
	this.movieReader = Objects.requireNonNull(movieReader);
}
```

##### 5-1. 중복 코드 발생 문제 및 객체 생성 방법
- ✅ `Factory` 
	- 객체의 생성 방법을 결정하고 생성한 객체를 반환하는 역할을 수행하는 객체
	- 주로 객체를 생성하는 쪽과 생성된 객체를 사용하는 쪽의 역할과 책임을 분리하려는 목적으로 사용

```java
// 객체 생성, 구성하는 역할 수행
public class MovieBuddyFactory {
	
	public MovieFinder movieFinder() {
		return new MovieFinder(new CsvMovieReader());
	}
}
```

#### 6. 제어의 역전(IoC)
- `기존 흐름`
	- 프로그램이 시작되는 지점에서 사용할 객체를 결정하고 결정한 객체를 생성하고 그리고 생성된 객체에 있는 메서드를 호출
	- 이후 호출된 객체를 결정, 생성, 호출하는 식으로 반복
	- ✅ 각 객체는 사용할 객체를 구성하는 작업에 `능동적`으로 참여!
	
### 🍃 스프링
> 스프링은 다양한 어노테이션 지원!!
#### ⭐ `애노테이션`
- 자바 5버전에 등장
- 자바코드의 메타데이터로 컴포일 또는 런타임에 활용
- 종류
	1. 자바 플랫폼이 제공하는 `빌트인 어노테이션`
	2. 개발자가 직접 작성할 수 있는 `커스텀 어노테이션`
- 스프링은 다양한 커스텀 어노테이션 제공!

#### 1. 스프링 IoC 컨테이너와 빈

- 의존성 추가

```
implementation 'org.springframework:spring-context:5.3.13'
implementation 'org.springframework:spring-context-support:5.3.13'
```

- 스프링 빈 구성 정보 설정
	- `@Configuration`
	- `@Bean`
	
- 빈 구성정보
	- 스프링컨테이너가 빈 객체 생성 및 구성, 조립시 사용하는 설정정보
	- 빈 : 컨테이너에 의해 생성 및 조립된 후 관리되는 객체
- 자바 코드로 작성하는 빈 정보
	- 관심사가 같은 컴포넌트들을 함께 묶어 __모듈화__ 가능
	- 여러개 빈 정보 작성 후 조합가능

```java
@Configuration
@Import({MovieBuddyFactory.DomainModuleConfig.class, MovieBuddyFactory.DataSourceModuleConfig.class}) // 다른 클래스에서 빈 정보 불러오기 위해 사용
public class MovieBuddyFactory {
		
	@Configuration
	static class DomainModuleConfig {
		@Bean
		// @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
		public MovieFinder movieFinder(MovieReader movieReader) {
			return new MovieFinder(movieReader);
		}
	}
	
	@Configuration
	static class DataSourceModuleConfig {
		@Bean
		public MovieReader movieReader() {
			return new CsvMovieReader();
		}
	}
}

```

#### 2. `자바 코드` 로 의존관계 주입
- 자바 기반 컨테이너 구성
	- 자바 코드를 빈 설정용 DSL로 사용

1. `메서드 콜`
	- 메서드를 호출해서 의존관계 주입하는 방식

```java
@Bean
public MovieReader movieReader() {
	return new CsvMovieReader();
}

@Bean
public MovieFinder movieFinder() {
	return new MovieFinder( ✅ movieReader());
}
```

2. `메서드 파라미터` 
	- 파라미터로 필요한 의존 관계를 받아오는 것

 ```java
@Bean
public MovieReader movieReader() {
	return new CsvMovieReader();
}

@Bean 
public MovieFinder movieFinder(✅ MovieReader movieReader) {
	return new MovieFinder(movieReader);
}
 ```

3. 생성자, 어노테이션 방식도 존재 ...

#### 3. 스프링 빈 스코프
- 스프링 컨테이너는 빈을 생성할 때 단 하나의 빈을 만들지, 빈이 요청될 때마다 새로운 빈 객체를 생성할지 결정하는 메커니즘을 가지고 있다. 이 매커니즘을 빈 스코프
- `싱글톤 스코프` (따로 설정이 없으면 이게 기본값)
	- 단 하나의 빈 객체만 만들 때
	- 스프링 컨테이너가 시작될 때 생성, 스프링 컨테이너가 종료될 때 소멸
- `프로토타입 스코프`
	- 빈이 요청될때마다 새로운 빈을 생성할 때

```java
@Bean
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE) ✅ >> @Scope("prototype") 이렇게 써도 되✅
public MovieFinder movieFinder(MovieReader movieReader) {
	return new MovieFinder(movieReader);
}
```

#### 4. `어노테이션` 으로 의존관계 주입
- ✅ 등록할 빈이 많아지면 기존 의존관계 주입으로 한계
- 스프링에서는 `스프링 컨테이너` 에 빈을 자동으로 등록해주는 기능 제공!
	- `@ComponentScan`로 활성화 가능(자동 클래스 탐지 기능) >> 자신 기준 패키지를 통해 탐색
		- ✅ 따로 패키지 기준을 설정하기 위해선 `@ComponentScan(basePackage = { "moviebuddy" }`
	- `@Component` 로 클래스 설정(빈)

- 이외 어노테이션 
	- `@Service` : 다른 빈이 필요로 하는 서비스를 제공하는, 보다 복잡한 비즈니스 기능을 가진 빈을 정의하는데 사용
	- `@Repository` : 데이터베이스와 같이 데이터 접근 기술이 사용하는 빈을 정의하는데 사용
	
#### 5. 자동으로 의존관계 설정(자동 와이어링 기법)
- ✅`@Autowired` : "아 의존 관계 주입이 필요하구나." 이해 / 생성자가 하나면 생략 가능
	- 생성자, 세터, 필드에서 사용가능
	
```java
	@Autowired ✅ 테스트가 어려워지기 때문에 운영 소스에서는 절대 사용하지 않는 것을 권장!(필드레벨) 
	MovieFinder movieFinder;
	
//	@Autowired
//	MovieBuddyFinderTest(MovieFinder movieFinder) {
//		this.movieFinder = movieFinder;
//	}
	
//	@Autowired
//	void setMovieFinder(MovieFinder movieFinder) {
//		this.movieFinder = movieFinder;
//	}
```

```java
@Autowired
public MovieFinder(MovieReader movieReader) {
	this.movieReader = Objects.requireNonNull(movieReader);
}
```

- 처음은 타입을 기반으로 해결
- 같은 타입이라면 이름으로 의존 관계를 구성하는 전략
	- `@Repository("movieReader")` 
	- `@Qualifier("csvMovieReader")`

#### 6. ✅테스트 컨텍스트 프레임워크
1. 의존성 추가
	- `testImplementation 'org.springframework:spring-test:5.3.9'`

2. 테스트 코드
	- `@ExtendWith` : JUnit이 테스트 실행 전략을 확장할 때 사용하는 어노테이션
	- `SpringExtension.class` : JUnit 지원 클래스, JUnit이 테스트를 실행하는 과정에서 테스트가 필요로 하는 스프링 컨테이너를 구성하고 관리 
	- `@ContextConfiguration(classes = MovieBuddyFactory.class)` : 해당 정보를 바탕으로 빈 구성한 스프링 컨테이너 만든다
	
3. `@SpringJUnitConfig(MovieBuddyFactory.class)`
	- `메타 어노테이션` : 어노테이션을 다른 에노테이션으로 만들 때 쓰는 방법

```
@ExtendWith(SpringExtension.class)
@ContextConfiguration
@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
```

4. 코드

```java
@SpringJUnitConfig(MovieBuddyFactory.class)✅
public class JaxbMovieReaderTest {

	@Autowired JaxbMovieReader jaxbMovieReader; ✅
	
	@Test
	void NotEmpty_LoadedMovies() {
		
		List<Movie> movies = jaxbMovieReader.loadMovies();
		// movies.size() = >> XML 문서에 등록된 영화 수와 동일한가
		Assertions.assertEquals(1375, movies.size());

	}
}

```

#### 7. 데이터원본 계층
- 데이터원본 계층으로 분리
	- 클라이언트에 속한 패키지에 포함하는 구조 >> `분리된 인터페이스 패턴(Separated Interface Pattern)`

##### 객체
- 애플리케이션 기능을 구현하기 위해서 협력에 참여하는 객체들 사이의 상호 작용
- 객체들은 협력에 참여하기 위해 역할을 부여받고 역할에 적합한 책임을 수행

---

#### ✅ 스프링의 프로파일 기능
	- 환경에 따른 빈 구성 기능을 제공
	- 해당 프로젝트는 사용자에 따라 csv, xml문서를 읽고 동작하는 상황(고객별 상황)

- `CsvMovieReader.java`

```java
@Profile(MovieBuddyProfile.CSV_MODE)✅
@Repository
public class CsvMovieReader implements MovieReader {
	
	@Override
    public List<Movie> loadMovies() {
    	
    	
        try {
            final URI resourceUri = ClassLoader.getSystemResource("movie_metadata.csv").toURI();
            final Path data = Path.of(FileSystemUtils.checkFileSystem(resourceUri));
            final Function<String, Movie> mapCsv = csv -> {
                try {
                    // split with comma
                    String[] values = csv.split(",");
                    ...
                    ...

```

- `JaxbMovieReader.java`

```java
@Profile(MovieBuddyProfile.XML_MODE) ✅
@Repository
public class JaxbMovieReader implements MovieReader {
	
	@Override
	public List<Movie> loadMovies() {
		try {
			final JAXBContext jaxb = JAXBContext.newInstance(MovieMetadata.class);
	        final Unmarshaller unmarshaller = jaxb.createUnmarshaller();

	        final InputStream content = ClassLoader.getSystemResourceAsStream("movie_metadata.xml");
	        final Source source = new StreamSource(content);
	        final MovieMetadata metadata = (MovieMetadata) unmarshaller.unmarshal(source);

			return metadata.toMovies();
		} catch (JAXBException error) {
		...
```

- test 사용시

```java
@ActiveProfiles(MovieBuddyProfile.CSV_MODE) ✅ 
@SpringJUnitConfig(MovieBuddyFactory.class)
//@ExtendWith(SpringExtension.class)
//@ContextConfiguration(classes = MovieBuddyFactory.class)
public class MovieBuddyFinderTest {
	
	
	@Autowired
	MovieFinder movieFinder;
	...
	...
	
```

- Profile 활성화 시키기 ✅
	- 공식 문서 사용
	

#### ✅ 스프링의 로그
- 스프링 5.0부터는 스프링 JCL 모듈을 통해 자바 프로젝트에서 많이 사용되는 로깅 프레임워크인 `SLF4J` 와 `Log4J 2.0 API`를 지원

##### SLF4J (Simple Logging Facade for Java)
- 여러 로깅 프레임워크를 추상화해서 만들어진 인터페이스 모음
- `파사드 패턴` 을 이용한 로깅 프레임워크
- 추상 로깅 프레임워크이기에 단독으로 로깅 불가능 ❌
	- 보통 네이티브 구현체인 `Logback` , `Log4J` 와 같은 구현체를 이용해 사용
	
- 의존성 추가

```
implementation 'ch.qos.logback:logback-classic:1.2.3'
```

- 결과

```
16:40:55.819 [main] DEBUG org.springframework.core.env.StandardEnvironment - Activating profiles [csv_mode]
```

#### ✅ 스프링 `OXM` 기술 >> __이식 가능한 서비스 추상화__
- 자바 객체와 XML을 매핑해서 상호 변환해 주는 기술을  `Object XML Mapping` 이라고 부른다
- 스프링이 제공하는 OXM 추상화 계층을 이용해 자바 객체와 XML 문서 사이의 변환을 처리하면 코드 수정 없이도 OXM 기술을 자유롭게 바꿔서 적용 가능
- 기술 종류 : `JAXB` `JIBX` `XStream` 
---
- 기존 특정 기술이였던 JAXB 기술이 아닌 OXM 기술로 변경 ✅✅

1. 의존성 추가

```
implementation 'org.springframework:spring-oxm:5.3.9'
``` 

2. __Umarshaller__ `org.spring.oxm.Unmarsharller`

3. Jaxb2Marshaller를 Bean 등록 

## 요구사항 두번째
#### `CsvMovieReader` `XmlMovieReader` 코드 변경해야되지만 ... ✅ 외부 주입하는 방식(설정자)으로 해결가능!

```java
private String metadata;

public String getMetadata() {
	return metadata;
}

public void setMetadata(String metadata) {
	this.metadata = Objects.requireNonNull(metadata, "metadata is required value");
}

``` 

- 문제점
	- 메타데이터가 올바르게 설정 됬는지 확인 불가능
	- ✅해결책1 : 세터를 통해 즉시 검증하는 코드 작성
	- ✅해결책2 : 빈 생명주기 확장 지점 `JSR 250`


#### 1. 애플리케이션 설정 외부화 >> 스프링 런타임 환경 추상화
1. JVM 프로퍼티를 통해 제공
	- 실행 시 VM 아규먼트 지정
	- 실행 아이콘 클릭 후 > `run configurations`
	- 코드를 변경하지 않아도 얼마든지 설정 정보를 작성하는 방식을 변경 가능	
2. ✅ 스프링은 위와 같은 저수준기술이 구현된 `Environment` 인터페이스를 제공 
3. 어노테이션 `@Value`을 통해 제공 
	- 쉽게 외부에서 설정 정보 값을 의존 관계 주입을 할 수가 있다
4. `properties` 파일로 메타데이터 설정

#### 2. 리소스 추상화 (스프링에서 제공)
- 기존 문제점
	- __클래스 패스__ 경로 상에 있는 자원만 처리
	- 애플리케이션 외부는 어떻게 읽지?? ✅ (네트워크상에 따른 파일)
	 
- `Resource` 인터페이스 사용

```java
Resource resource = getMetadataResource();
if (!resource.exists()) {
	throw new FileNotFoundException(metadata); 
}

if (!resource.isReadable()) {
	throw new ApplicationException(String.format("cannot read to metadata [%s]", metadata));
}	
```


## 요구사항 세번째
- 기존 문제점
	- 실행 속도가 느려짐
- 매번 내려받지 않고 한번만 내려받기! ✅ >> `Cache`

#### 1. 캐시
- 자주 사용하는 데이터나 값을 미리 복사해 높는 임시 저장소
- 운영 체제, CDN, DNS 등의 네트워크 계층 / 애플리케이션 / 데이터베이스 비롯해서 다양한 기술 계층에서 적용되서 활용 가능
- 지연 시간 줄이고, 컴퓨팅의 성능 향상
- 라이브러리
	- 자바 8 기반 고성능 캐시 라이브러리: __카페인__ ✅✅
	- `implementation 'com.github.ben-manes.caffeine:caffeine:2.8.0'`
- 캐시에 사용한 메모리 크기 ✅✅
	- 애플리케이션이 동작하는 데 영향을 주지 않을 정도로 적절한 크기여야 함 >> 캐시 관리

```java
@Test
void useCache() throws InterruptedException {
	Cache<String, Object> cache =  Caffeine.newBuilder()
			.expireAfterWrite(200, TimeUnit.MILLISECONDS)
			.maximumSize(100)
			.build();
	
	String key = "springrunner";
	Object value = new Object();
	
	Assertions.assertNull(cache.getIfPresent(key));
	
	cache.put(key, value);
	Assertions.assertEquals(value, cache.getIfPresent(key));
	
	TimeUnit.MILLISECONDS.sleep(100);
	Assertions.assertEquals(value, cache.getIfPresent(key));
	
	TimeUnit.MILLISECONDS.sleep(100);
	Assertions.assertNull(cache.getIfPresent(key)); // 만료되기 때문
}
```
- 카페인 라이브러리 문제점 ✅✅
	- 향후 변경이 어렵게 됨 >> __서비스 추상화__ 해야된다!
	
#### 2. AOP `데코레이터 패턴`
- 캐시 적용시 문제점
	- 중복 코드가 많아지게 됨 >> `AOP` 기술로 해결 가능!
