package moviebuddy;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import moviebuddy.data.AbstractFileMetadataResourceMovieReader;
import moviebuddy.data.CacheMovieReader;
import moviebuddy.data.CsvMovieReader;
import moviebuddy.data.XmlMovieReader;
import moviebuddy.domain.Movie;
import moviebuddy.domain.MovieReader;

// 객체 생성, 구성하는 역할 수행

@Configuration
@PropertySource("/application.properties")
@ComponentScan
@Import({MovieBuddyFactory.DomainModuleConfig.class, MovieBuddyFactory.DataSourceModuleConfig.class}) // 다른 클래스에서 빈 정보 불러오기 위해 사용
public class MovieBuddyFactory {

	
	@Bean
	public Jaxb2Marshaller jaxb2Marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setPackagesToScan("moviebuddy");
		
		return marshaller;
	}
		
	@Configuration
	static class DomainModuleConfig {
		
	}
	
	@Configuration
	static class DataSourceModuleConfig {	
		@Primary
		@Bean
		public MovieReader cachingMovieReader(CacheManager cacheManager, MovieReader target) {
			return new CacheMovieReader(cacheManager, target);
		}
	}
	
}
