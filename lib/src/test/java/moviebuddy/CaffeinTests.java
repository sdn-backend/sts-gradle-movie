package moviebuddy;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

public class CaffeinTests {
	
	@Test
	void useCache() throws InterruptedException {
		Cache<String, Object> cache =  Caffeine.newBuilder()
				.expireAfterWrite(200, TimeUnit.MILLISECONDS)
				.maximumSize(100)
				.build();
		
		String key = "springrunner";
		Object value = new Object();
		
		Assertions.assertNull(cache.getIfPresent(key));
		
		cache.put(key, value);
		Assertions.assertEquals(value, cache.getIfPresent(key));
		
		TimeUnit.MILLISECONDS.sleep(100);
		Assertions.assertEquals(value, cache.getIfPresent(key));
		
		TimeUnit.MILLISECONDS.sleep(100);
		Assertions.assertNull(cache.getIfPresent(key)); // 만료되기 때문
	}
	

}
