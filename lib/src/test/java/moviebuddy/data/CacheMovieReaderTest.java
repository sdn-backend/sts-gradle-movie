package moviebuddy.data;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.cache.CacheManager;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;

import moviebuddy.domain.Movie;
import moviebuddy.domain.MovieReader;

public class CacheMovieReaderTest {

	
	@Test
	void caching() {
		CacheManager cacheManager = new ConcurrentMapCacheManager();
		MovieReader target = new DummyMovieReader();
		
		CacheMovieReader cacheMovieReader = new CacheMovieReader(cacheManager, target);
		
		Assertions.assertNull(cacheMovieReader.getCachedData());
		
		List<Movie> movies = cacheMovieReader.loadMovies();
		Assertions.assertNotNull(cacheMovieReader.getCachedData());
		Assertions.assertSame(cacheMovieReader.loadMovies(), movies);
	}
	
	class DummyMovieReader implements MovieReader {

		@Override
		public List<Movie> loadMovies() {
			// TODO Auto-generated method stub
			return new ArrayList<>();
		}
		
	}
}
